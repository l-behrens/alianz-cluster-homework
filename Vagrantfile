# encoding: utf-8
# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'

GUEST_PATH = '/vagrant'
HOST_PATH = './jobs'

VM_USER = 'nomad'

ANSIBLE_INVENTORY = "hosts_vagrant"
ANSIBLE_PATH = "./ansible"

boxes = YAML.load_file(File.join(File.dirname(__FILE__), 'boxes.yml'))


Vagrant.configure('2') do |config|
  local_domainname = 'local'
  config.vm.synced_folder HOST_PATH, GUEST_PATH
  config.vm.synced_folder '.', '/home/'+VM_USER+'', disabled: true

  boxes.each do |box|

    config.vm.box = "#{box["image"]}"
    config.vm.define "#{box["hostname"]}", autostart: box["start"] do |node|

      local_hostname = "#{box["hostname"]}"
      node.vm.hostname = "#{local_hostname}.#{local_domainname}"
      node.vm.network :private_network, ip: "#{box["ip"]}"
      node.vm.provider 'virtualbox' do |vb|

        vb.name   = "#{box["hostname"]}"
        vb.memory = "#{box["memory"]}"
        vb.cpus   = "#{box["cpus"]}"
      end
    end
  end
end

File.open("#{ANSIBLE_PATH}#{ANSIBLE_INVENTORY}" ,'w') do | f |
  boxes.each do |box|
    if "#{box["hostname"]}".include? "master"
      f.write "#{box["hostname"]} ansible_host=#{box["ip"]} ansible_ssh_host=#{box["ip"]} ansible_user=vagrant ansible_ssh_common_args='-o StrictHostKeyChecking=no' ansible_ssh_private_key_file=.vagrant/machines/#{box["hostname"]}/virtualbox/private_key consul_bootstrap=true\n"
    else
      f.write "#{box["hostname"]} ansible_host=#{box["ip"]} ansible_ssh_host=#{box["ip"]} ansible_user=vagrant ansible_ssh_common_args='-o StrictHostKeyChecking=no' ansible_ssh_private_key_file=.vagrant/machines/#{box["hostname"]}/virtualbox/private_key\n"
    end
  end
  f.write "\n"
  f.write "[server]\n"
  boxes.each do |box|
    if "#{box["hostname"]}".include? "master"
      f.write "#{box["hostname"]}\n"
    end
  end
  f.write "\n"
  f.write "[client]\n"
  boxes.each do |box|
    if "#{box["hostname"]}".include? "worker"
    f.write "#{box["hostname"]}\n"
    end
  end
  f.write "\n"
  f.write "[client:vars]\n"
  f.write "nomad_client=true\n"
  f.write "nomad_server=false\n"
  f.write "consul_client=true\n"
  f.write "consul_server=false\n"
end

Vagrant.configure("2") do |config|
  #
  # Run Ansible from the Vagrant Host
  #
  config.vm.provision "ansible" do |ansible|
    ansible.inventory_path = "#{ANSIBLE_PATH}/#{ANSIBLE_INVENTORY}"
    ansible.playbook = "#{ANSIBLE_PATH}/playbook.yml"
  end

end
