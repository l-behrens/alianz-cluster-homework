terraform {
  required_providers {
    vagrant = {
      source = "bmatcuk/vagrant"
      version = "0.0.0"
    }
  }
}

provider "vagrant" {
  # Configuration options
}

resource "vagrant_vm" "my_vagrant_vm" {
  vagrantfile_dir = "."
  env = {
      VAGRANTFILE_HASH = md5(file("./Vagrantfile"))
  }
  get_ports = true
}
